import XCTest
@testable import Extensions

final class ExtensionsTests: XCTestCase {
    func testExample() throws {
        var test = "test"
        test[0] = test[0]?.uppercased()
        XCTAssertEqual(test, "Test")
    }
}
