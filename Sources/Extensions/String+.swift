import Foundation

public extension String {
    subscript(offset: Int) -> String? {
        get {
            (0..<count).contains(offset) ? String(self[index(startIndex, offsetBy: offset)]) : nil
        }
        set(newValue) {
            guard (0..<count).contains(offset),
                  let newValue = newValue else {
                return
            }
            let prefix = prefix(offset)
            let suffix = suffix(count - offset - 1)
            self = prefix + newValue + suffix
        }
    }
}
